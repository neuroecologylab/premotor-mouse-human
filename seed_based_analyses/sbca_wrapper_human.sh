#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch
scriptDir=/vols/Scratch/comparative_scripts
mkdir -p $workDir/connectivity_maps_humans

subjList="HCP01
HCP02
HCP03
HCP04
HCP05
HCP06
HCP07
HCP08
HCP09
HCP10
HCP11
HCP12
HCP13
HCP14
HCP15
HCP16
HCP17
HCP18
HCP19
HCP20"

seedList=""

premotorList=""

template="$FSLDIR"/data/standard/MNI152_T1_2mm_brain

for subj in $subjList; do

  echo "SBCA for $subj"

  for seed in $premotorList; do

  echo "seed $seed"
  fsl_sub -q veryshort.q -l $scriptDir/logs fsl_sbca -i $workDir/HCP_data/"$subj"/MNINonLinear/Results/rfMRI_REST1_LR/rfMRI_REST1_LR_hp2000_clean.nii.gz -s $template  -t $scriptDir/m2_homologies/seeds/"$seed"_human_seed.nii.gz -o $workDir/connectivity_maps_humans/"$subj"_"$seed"_map

  done

done

echo "done"
