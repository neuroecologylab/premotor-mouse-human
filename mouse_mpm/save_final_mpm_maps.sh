#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/alazari/comparative_scripts/mouse_MPM_results
outDir=/vols/Scratch/alazari/comparative_scripts/mouse_MPM_results/final_files

subjScan="20210517_175914_AL2_MPM_1_1@AL2
20210604_181641_AL3_AL3_MPM_3_1_3@AL3
20210609_200948_AL5_MPM_1_3@AL5
20210607_192507_AL6_MPM_4_1_6@AL6
20210611_205425_AL7_MPM_2_1_3@AL7
20210402_231711_AL8_AL8_MPM_1_3@AL8
20210603_165354_AL11_AL11_MPM_2_1_3@AL11
20210617_183610_AL13_MPM_1_3@AL13
20210614_185959_AL15_MPM_1_2@AL15
20210616_175000_AL16_MPM_2_1_2@AL16
20210608_164021_AL18_MPM_1_3@AL18
20210618_202710_AL19_MPM_1_1@AL19"

maptype="MT R1 R2"
mkdir -p $outDir/MT
mkdir -p $outDir/R1
mkdir -p $outDir/R2

for subjScan in $subjScan; do

subj=${subjScan%@*}
scan=${subjScan#*@}
echo "$subj"
echo "$scan"

for map in $maptype; do
echo "$map"

  cp $workDir/"$map"/"$subj"_"$map"_registered.nii.gz $outDir/"$map"/"$scan".nii

done

echo "done"

done
