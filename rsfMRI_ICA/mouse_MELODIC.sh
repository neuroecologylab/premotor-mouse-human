#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch
scriptDir=/vols/Scratchcomparative_scripts
outDir=/vols/Scratch/comparative_scripts/gICA
mkdir -p $outDir

subjList="001
002
003
004
005
006
007
008
009
010
011
012
013
014
015
016
017
018
019
020"

template=/vols/Scratch/Valerio_data/Template/QBI_atlas_2mm_v6.nii

for subj in $subjList; do
  echo "$subj"

  echo "$workDir/Valerio_data/Data/"$subj"_FIXed_QBI.nii.gz" >> $scriptDir/m2_homologies/mouse_fMRI_dirs.txt

done

echo "done"

fsl_sub -q verylong.q melodic -i $scriptDir/m2_homologies/mouse_fMRI_dirs.txt -o $outDir/groupICA_30 \
    --tr=1 --nobet -a concat \
    -m $template \
    --report --Oall -d 30
